import settings


class Colors:
    def __init__(self):
        self.bg = "#1d2021"
        self.bglight = "#32302f"
        self.fg = "#D4BE98"
        self.fgdark = "#a89984"
        self.green = "#a9b665"
        self.red = "#ea6962"
        self.yellow = "#d8a657"
        self.blue = "#7daea3"
        self.purple = "#d3869b"
        self.aqua = "#89b482"
        # transparent color
        self.trans = "#00000000"
        # should use these
        self.background = self.bg
        self.foreground = self.fg
        # used to easily switch between transparent bar and opaque
        if settings.trans_back:
            self.background = self.trans
            self.blank = self.trans
        else:
            self.blank = self.bg
