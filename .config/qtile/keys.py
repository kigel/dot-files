from libqtile.config import Key
from libqtile.lazy import lazy


def make_keys(mod, terminal, app_launcher):
    my_keys = MyKeys(mod, terminal, app_launcher)
    return my_keys.init_keys()


class MyKeys:
    def __init__(self, mod, terminal, app_launcher):
        self.mod = mod
        self.terminal = terminal
        self.app_launcher = app_launcher

    def init_keys(self):
        return [
            # A list of available commands that can be bound to keys can be found
            # at https://docs.qtile.org/en/latest/manual/config/lazy.html
            Key([self.mod], "h", lazy.layout.left(), desc="Move focus to left"),
            Key([self.mod], "l", lazy.layout.right(), desc="Move focus to right"),
            Key([self.mod], "j", lazy.layout.down(), desc="Move focus down"),
            Key([self.mod], "k", lazy.layout.up(), desc="Move focus up"),
            Key(
                [self.mod, "shift"],
                "h",
                lazy.layout.shuffle_left(),
                desc="Move window to the left",
            ),
            Key(
                [self.mod, "shift"],
                "l",
                lazy.layout.shuffle_right(),
                desc="Move window to the right",
            ),
            Key(
                [self.mod, "shift"],
                "j",
                lazy.layout.shuffle_down(),
                desc="Move window down",
            ),
            Key(
                [self.mod, "shift"],
                "k",
                lazy.layout.shuffle_up(),
                desc="Move window up",
            ),
            Key(
                [self.mod],
                "h",
                lazy.layout.shrink_main(),
                desc="Shrink Main Section",
            ),
            Key(
                [self.mod],
                "l",
                lazy.layout.grow_main(),
                desc="Grow Main Section",
            ),
            Key(
                [self.mod], "Return", lazy.spawn(self.terminal), desc="Launch terminal"
            ),
            Key([self.mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
            Key([self.mod], "w", lazy.window.kill(), desc="Kill focused window"),
            Key(
                [self.mod],
                "m",
                lazy.window.toggle_fullscreen(),
                desc="Toggle fullscreen on the focused window",
            ),
            Key(
                [self.mod],
                "f",
                lazy.window.toggle_floating(),
                desc="Toggle floating on the focused window",
            ),
            Key(
                [self.mod, "control"],
                "r",
                lazy.reload_config(),
                desc="Reload the config",
            ),
            Key([self.mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
            Key(
                [self.mod],
                "r",
                lazy.spawn(self.app_launcher),
                desc="Spawn an app launcher",
            ),
        ]
