import os
import subprocess

from widgets import make_widgets
from keys import make_keys
from colors import Colors
import settings

from libqtile import layout, hook
from libqtile.config import Click, Drag, Group, Key, Match, Screen
from libqtile.lazy import lazy

colors = Colors()

#              _            _             _
#   __ _ _   _| |_ ___  ___| |_ __ _ _ __| |_
#  / _` | | | | __/ _ \/ __| __/ _` | '__| __|
# | (_| | |_| | || (_) \__ \ || (_| | |  | |_
#  \__,_|\__,_|\__\___/|___/\__\__,_|_|   \__|


@hook.subscribe.startup_once
def autostart():
    script = os.path.expanduser("~/.config/qtile/autorun.sh")
    subprocess.run([script])


#  _
# | | _____ _   _ ___
# | |/ / _ \ | | / __|
# |   <  __/ |_| \__ \
# |_|\_\___|\__, |___/
#           |___/

keys = make_keys(settings.mod, settings.terminal, settings.app_launcher)

#           _     _            _
# __      _(_) __| | __ _  ___| |_ ___
# \ \ /\ / / |/ _` |/ _` |/ _ \ __/ __|
#  \ V  V /| | (_| | (_| |  __/ |_\__ \
#   \_/\_/ |_|\__,_|\__, |\___|\__|___/
#                   |___/


if settings.bar_gap:
    screens, widget_defaults = make_widgets(settings.bar_height, settings.gap)
else:
    screens, widget_defaults = make_widgets(settings.bar_height, 0)

extension_defaults = widget_defaults.copy()


group_names = [
    "",
    "󰖟",
    "",
    "",
    "󰭹",
    "",
    "",
]

groups = [Group(i) for i in group_names]

group_keys = "1234567"

for i, key in zip(groups, group_keys):
    keys.extend(
        [
            Key(
                [settings.mod],
                key,
                lazy.group[i.name].toscreen(),
                desc="Switch to group {}".format(i.name),
            ),
            Key(
                [settings.mod, "shift"],
                key,
                lazy.window.togroup(i.name),
                desc="move focused window to group {}".format(i.name),
            ),
        ]
    )

layouts = [
    layout.MonadTall(
        margin=settings.gap,
        ratio=0.55,
        border_width=1,
        border_normal=colors.bg,
        border_focus=colors.fg,
    ),
    layout.Max(),
]

# Drag floating layouts.
mouse = [
    Drag(
        [settings.mod],
        "Button1",
        lazy.window.set_position_floating(),
        start=lazy.window.get_position(),
    ),
    Drag(
        [settings.mod],
        "Button3",
        lazy.window.set_size_floating(),
        start=lazy.window.get_size(),
    ),
    Click([settings.mod], "Button2", lazy.window.bring_to_front()),
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: list
follow_mouse_focus = True
bring_front_click = False
floats_kept_above = True
cursor_warp = False
floating_layout = layout.Floating(
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        Match(wm_class="ssh-askpass"),  # ssh-askpass
        Match(title="branchdialog"),  # gitk
        Match(title="pinentry"),  # GPG key password entry
    ]
)
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

# When using the Wayland backend, this can be used to configure input devices.
wl_input_rules = None

wmname = "LG3D"
