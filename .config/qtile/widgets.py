from libqtile import bar
from qtile_extras import widget
from qtile_extras.widget.decorations import BorderDecoration, RectDecoration
from libqtile.config import Screen
from colors import Colors
import settings


def make_widgets(size, margin):
    my_widgets = MyWidgets()
    widget_defaults = dict(
        font="monospace",
        fontsize=16,
        background=my_widgets.colors.background,
        foreground=my_widgets.colors.fg,
        padding=0,
        padding_x=0,
        padding_y=0,
        margin=0,
        margin_x=0,
        margin_y=0,
    )
    return my_widgets.init_screens(size, margin), widget_defaults


class MyWidgets:
    def __init__(self):
        self.colors = Colors()
        self.sep_width = 0

        if not settings.bar_gap:
            self.sep_width = settings.gap

    def make_bottom_line(self, color):
        return {
            "decorations": [
                BorderDecoration(
                    border_width=[0, 0, 3, 0],
                    colour=color,
                )
            ]
        }

    def make_nice_box(self):
        if not settings.trans_back:
            return {}

        return {
            "decorations": [
                RectDecoration(
                    colour=self.colors.bg,
                    radius=10,
                    filled=True,
                    clip=True,
                    padding=0,
                    group=True,
                )
            ],
        }

    def make_nice_box_and_bottom_line(self, color):
        if not settings.trans_back:
            return self.make_bottom_line(color)

        bottom_line = self.make_bottom_line(color)
        nice_box = self.make_nice_box()

        # Combine the two dictionaries
        combined_map = {
            "decorations": nice_box["decorations"] + bottom_line["decorations"]
        }

        return combined_map

    def init_widgets_list(self):
        """
        Function that returns the desired widgets in form of list
        """
        widgets_list = [
            widget.Sep(
                background=self.colors.blank,
                foreground=self.colors.blank,
                linewidth=self.sep_width,
            ),
            widget.GroupBox(
                fmt="{}",
                use_mouse_wheel=False,
                scroll=False,
                rounded=False,
                highlight_method="line",
                hide_unused=True,
                padding_y=20,
                padding_x=10,
                margin_x=self.sep_width,
                margin_y=5,
                active=self.colors.fg,
                inactive=self.colors.fg,
                highlight_color=self.colors.background,
                this_current_screen_border=self.colors.green,
                other_current_screen_border=self.colors.bglight,
                this_screen_border=self.colors.green,
                other_screen_border=self.colors.bglight,
                **(self.make_nice_box()),
            ),
            widget.Spacer(
                background=self.colors.blank,
            ),
            widget.WindowName(
                width=bar.CALCULATED,
                empty_group_string="",
                padding=10,
                max_chars=130,
                **(self.make_nice_box()),
            ),
            # Is used to balance the positioning of the window name
            widget.Sep(
                background=self.colors.blank,
                foreground=self.colors.blank,
                linewidth=50,
            ),
            widget.Spacer(
                background=self.colors.blank,
            ),
            widget.GenPollCommand(
                fmt="<span color='" + self.colors.blue + "'></span> {}",
                cmd="setxkbmap -query | awk '/layout:/ {print $2}' | tr '[:lower:]' '[:upper:]'",
                update_interval=0.1,
                padding=10,
                shell=True,
                **(self.make_nice_box_and_bottom_line(self.colors.blue)),
            ),
            widget.Sep(
                background=self.colors.blank,
                foreground=self.colors.blank,
                linewidth=self.sep_width,
            ),
            widget.Clock(
                fmt="<span color='" + self.colors.yellow + "'>󰥔</span> {}",
                format="%H:%M %a",
                padding=10,
                **(self.make_nice_box_and_bottom_line(self.colors.yellow)),
            ),
            widget.Sep(
                background=self.colors.blank,
                foreground=self.colors.blank,
                linewidth=self.sep_width,
            ),
        ]
        return widgets_list

    def init_widgets_screen(self):
        """
        Function that returns the widgets in a list.
        It can be modified so it is useful if you  have a multimonitor system
        """
        widgets_screen = self.init_widgets_list()
        return widgets_screen

    def init_widgets_screen2(self):
        """
        Function that returns the widgets in a list.
        It can be modified so it is useful if you  have a multimonitor system
        """
        widgets_screen2 = self.init_widgets_screen()
        return widgets_screen2

    def init_screens(self, size, margin):
        """
        Init the widgets in the screen
        """
        return [
            Screen(
                top=bar.Bar(
                    widgets=self.init_widgets_screen(),
                    background="#00000000",
                    size=size,
                    margin=[margin, margin, 0, margin],
                )
            ),
            Screen(
                top=bar.Bar(
                    widgets=self.init_widgets_screen2(),
                    background="#00000000",
                    size=size,
                    margin=[margin, margin, 0, margin],
                )
            ),
        ]
