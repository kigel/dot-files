{ lib, config, pkgs, inputs, ... }:

let
    cfg = config.wm.hyprland;
in
{
    options.wm.hyprland.enable = lib.mkEnableOption "enable hyprland";

    config = lib.mkIf cfg.enable {
        programs.hyprland = {
            enable = true;
            enableNvidiaPatches = true;
            xwayland.enable = true;
        };

        environment.systemPackages = with pkgs; [
            kitty
            waybar
            wlr-randr
            rofi-wayland
        ];

        environment.sessionVariables = {
            WLR_NO_HARDWARE_CURSORS = "1";
            NIXOS_OZONE_WL = "1";
        };
    };
}
