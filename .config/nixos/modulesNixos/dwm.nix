{ lib, config, pkgs, inputs, ... }:

let
    cfg = config.wm.dwm;
    dwmSrc = inputs.dwm-kigel;
    dmenuSrc = inputs.dmenu-kigel;
in
{
    options.wm.dwm.enable = lib.mkEnableOption "enable dwm";

    config = lib.mkIf cfg.enable {
        services.xserver.windowManager.dwm.enable = true;
        services.xserver.windowManager.dwm.package = pkgs.dwm.overrideAttrs (oldAttrs: {
            src = dwmSrc;
            buildInputs = (oldAttrs.buildInputs or []) ++ [ pkgs.fribidi pkgs.xorg.xcbutil ];
            nativeBuildInputs = (oldAttrs.nativeBuildInputs or []) ++ [ pkgs.pkg-config ];
        });

        nixpkgs.overlays = [
            (final: prev: {
                dmenu = prev.dmenu.overrideAttrs (old: { src = dmenuSrc; });
            }) 
        ];

        services.xserver.windowManager.session = [ 
          { 
            name = "dwm";
            start = ''
                export _JAVA_AWT_WM_NONREPARENTING=1
                autostart-dwm &

                while true; do
                    dwm 2>>"$XDG_CACHE_HOME"/dwm/dwm.log
                done
                # waitPID=$!
            '';
          }
        ];
    };
}
