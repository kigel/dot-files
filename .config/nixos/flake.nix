{
  description = "Nixos config flake";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-23.11";

    home-manager.url = "github:nix-community/home-manager/release-23.11";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";

    dwm-kigel.url = "gitlab:kigel-projects/dwm";
    dwm-kigel.flake = false;

    dmenu-kigel.url = "gitlab:kigel-projects/dmenu";
    dmenu-kigel.flake = false;
  };

  outputs = { self, nixpkgs, home-manager, ... }@inputs:
    let
      system = "x86_64-linux";
      pkgs = nixpkgs.legacyPackages.${system};
    in
    {
      nixosConfigurations = {
        kigel-nix-laptop = nixpkgs.lib.nixosSystem {
          specialArgs = { inherit inputs; };
          modules = [ ./hosts/kigel-nix-laptop/configuration.nix ];
        };
        kigel-nix-pc = nixpkgs.lib.nixosSystem {
          specialArgs = { inherit inputs; };
          modules = [ ./hosts/kigel-nix-pc/configuration.nix ];
        };
      };
      homeConfigurations = {
        kigel = home-manager.lib.homeManagerConfiguration {
          inherit pkgs;
          modules = [ ./home.nix ];
        };
      };
    };
}
