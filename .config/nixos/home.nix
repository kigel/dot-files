{ lib, config, pkgs, inputs, ... }:

{
  imports = [
    ./modulesHome
  ];

  home.username = "kigel";
  home.homeDirectory = "/home/kigel";
  home.stateVersion = "23.11";

  nixpkgs.config.allowUnfree = true;
  nixpkgs.config.permittedInsecurePackages = [ "electron-25.9.0" ];

  custom.programs = {
    wezterm.enable = true;
    lf.enable = true;
    feh.enable = true;
    picom.enable = true;
    sxhkd.enable = true;
    nvim.enable = true;
    zsh.enable = true;
    tmux.enable = true;
  };

  custom.other = {
    git.enable = true;
    gtk.enable = true;
    xrdb.enable = true;
  };

  home.file.".config/npm/npmrc".text = ''
    cache=~/.cache/npm
    init-module=~/.config/npm/config/npm-init.js 
  '';

  home.packages = with pkgs;[
    kitty
    alacritty
    dconf
    discord
    obsidian
    firefox
    brave
    lxappearance
    # the gay stuff
    neofetch
    pridefetch
    nitch
    pfetch-rs
  ];

  home.sessionVariables = {
    TERMINAL = "wezterm";
    OPENER = "xdg-open";
    EDITOR = "nvim";
    VISUAL = "nvim";

    WGETRC = "$XDG_CONFIG_HOME/wgetrc";
    HISTFILE = "$XDG_STATE_HOME/bash/history";
    NVM_DIR = "$XDG_DATA_HOME/nvm";
    NODE_REPL_HISTORY = "$XDG_DATA_HOME/node_repl_history";
    CARGO_HOME = "$XDG_DATA_HOME/cargo";
    PYTHONPYCACHEPREFIX = "$XDG_CACHE_HOME/python";
    PYTHONUSERBASE = "$XDG_DATA_HOME/python";
    GOPATH = "$XDG_DATA_HOME/go";
    GOMODCACHE = "$XDG_CACHE_HOME/go/mod";
  };

  home.shellAliases = {
    wget = "wget --hsts-file=\"$XDG_CACHE_HOME/wget-hsts\"";
    yarn = "yarn --use-yarnrc \"$XDG_CONFIG_HOME/yarn/config\"";
  };

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;
}
