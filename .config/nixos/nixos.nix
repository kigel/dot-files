{ lib, config, pkgs, inputs, ... }:

{
  imports = [
    ./modulesNixos
  ];

  #  ____              _   
  # | __ )  ___   ___ | |_ 
  # |  _ \ / _ \ / _ \| __|
  # | |_) | (_) | (_) | |_ 
  # |____/ \___/ \___/ \__|

  boot.kernelParams = [ "quiet" ];
  boot.loader = {
    efi = {
      canTouchEfiVariables = true;
    };
    grub = {
      enable = true;
      useOSProber = true;
      efiSupport = true;
      device = "nodev";
      theme = pkgs.sleek-grub-theme;
    };
  };

  # __  __  _   _ 
  # \ \/ / / | / |
  #  \  /  | | | |
  #  /  \  | | | |
  # /_/\_\ |_| |_|

  services.xserver.enable = true;

  services.xserver.displayManager = {
    sddm = {
      enable = true;
      # autoLogin.relogin = true;
    };
    autoLogin = {
      enable = true;
      user = "kigel";
    };
  };
  services.xserver.libinput = {
    enable = true;
    mouse.accelProfile = "flat";
  };

  wm.dwm.enable = true;
  # wm.hyprland.enable = true;

  #  _   _                      
  # | | | | ___  _ __ ___   ___ 
  # | |_| |/ _ \| '_ ` _ \ / _ \
  # |  _  | (_) | | | | | |  __/
  # |_| |_|\___/|_| |_| |_|\___|
  #                             

  users.users.kigel = {
    isNormalUser = true;
    extraGroups = [ "networkmanager" "wheel" ];
    initialPassword = "3250";
    shell = pkgs.zsh;
  };

  #   ____                           _   ____       _   _   _                 
  #  / ___| ___ _ __   ___ _ __ __ _| | / ___|  ___| |_| |_(_)_ __   __ _ ___ 
  # | |  _ / _ \ '_ \ / _ \ '__/ _` | | \___ \ / _ \ __| __| | '_ \ / _` / __|
  # | |_| |  __/ | | |  __/ | | (_| | |  ___) |  __/ |_| |_| | | | | (_| \__ \
  #  \____|\___|_| |_|\___|_|  \__,_|_| |____/ \___|\__|\__|_|_| |_|\__, |___/
  #                                                                 |___/     

  environment.variables = rec {
    XDG_CACHE_HOME = "$HOME/.cache";
    XDG_CONFIG_HOME = "$HOME/.config";
    XDG_DATA_HOME = "$HOME/.local/share";
    XDG_STATE_HOME = "$HOME/.local/state";
    XDG_BIN_HOME = "$HOME/.local/bin";
    NIXOS_CONFIG = "$HOME/.config/nixos";
    PATH = [
      "${XDG_BIN_HOME}"
      "${XDG_BIN_HOME}/statusbar"
    ];
  };

  programs.zsh.enable = true;

  nix.settings.experimental-features = [ "nix-command" "flakes" ];

  time.timeZone = "Asia/Jerusalem";

  i18n.defaultLocale = "en_US.UTF-8";

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "en_IL";
    LC_IDENTIFICATION = "en_IL";
    LC_MEASUREMENT = "en_IL";
    LC_MONETARY = "en_IL";
    LC_NAME = "en_IL";
    LC_NUMERIC = "en_IL";
    LC_PAPER = "en_IL";
    LC_TELEPHONE = "en_IL";
    LC_TIME = "en_IL";
  };

  services.printing.enable = true;

  sound.enable = true;
  hardware.pulseaudio.enable = false;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
  };

  #  ____  _  ______ ____  
  # |  _ \| |/ / ___/ ___| 
  # | |_) | ' / |  _\___ \ 
  # |  __/| . \ |_| |___) |
  # |_|   |_|\_\____|____/ 
  #                        

  nixpkgs.config.allowUnfree = true;
  nixpkgs.config.permittedInsecurePackages = [ "electron-25.9.0" ];

  programs._1password.enable = true;
  programs._1password-gui.enable = true;
  programs._1password-gui.polkitPolicyOwners = [ "kigel" ];

  environment.systemPackages = with pkgs; [
    wezterm
    lf
    feh
    picom
    sxhkd
    brave
    rustc
    cargo
    go
    dunst
    libnotify
    rofi
    python3
    nodejs_21
    playerctl
    tmux
    lua
    mpd
    bat
    gcc
    xbanish
    lxqt.lxqt-policykit
    figlet
    eza
    xclip
    kitty
    chafa
    delve
    pcmanfm
    gimp
    jq
    spotify
    sxhkd
    lf
    udiskie
    pulsemixer
    git
    shellcheck
    xcolor
    fzf
    unzip
    neofetch
    neovim
    dmenu
    update-nix-fetchgit
    nix-prefetch
    htop-vim
    bottom
    xdg-desktop-portal
    xdg-desktop-portal-gtk
  ];

  fonts = {
    packages = with pkgs; [
      noto-fonts
      noto-fonts-cjk
      noto-fonts-color-emoji
      fira-code
      fira-code-symbols
      jetbrains-mono
      roboto-mono
      hack-font
      meslo-lg
      nerdfonts
      font-awesome
      monaspace
    ];

    fontconfig = {
      defaultFonts = {
        sansSerif = [ "Noto Sans" "Noto Sans Hebrew" ];
        serif = [ "Noto Sans" "Noto Sans Hebrew" ];
        # monospace = [ "Hack" ];
        monospace = [ "JetBrains Mono" ];
        # monospace = [ "Roboto Mono" ];
      };
    };

  };

}
