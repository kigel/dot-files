{ config, lib, pkgs, ... }:

let
  cfg = config.custom.other.xrdb;
  background = "#1d2021";
  foreground = "#d4be98";
  black = "#32302f";
  red = "#ea6962";
  green = "#a9b665";
  yellow = "#d8a657";
  blue = "#7daea3";
  magenta = "#d3869b";
  cyan = "#89b482";
  grey = "#d4be98";
in
{
  options.custom.other.xrdb.enable = lib.mkEnableOption "enable xrdb";

  config = lib.mkIf cfg.enable {
    xresources.properties = {
      "*background" = "${background}";
      "*foreground" = "${foreground}";

      "*black" = "${black}";
      "*color0" = "${black}";
      "*color8" = "${black}";

      "*red" = "${red}";
      "*color1" = "${red}";
      "*color9" = "${red}";

      "*green" = "${green}";
      "*color2" = "${green}";
      "*color10" = "${green}";

      "*yellow" = "${yellow}";
      "*color3" = "${yellow}";
      "*color11" = "${yellow}";

      "*blue" = "${blue}";
      "*color4" = "${blue}";
      "*color12" = "${blue}";

      "*magenta" = "${magenta}";
      "*color5" = "${magenta}";
      "*color13" = "${magenta}";

      "*cyan" = "${cyan}";
      "*color6" = "${cyan}";
      "*color14" = "${cyan}";

      "*grey" = "${grey}";
      "*color7" = "${grey}";
      "*color15" = "${grey}";

      "dwm.bg" = "${background}";
      "dwm.fg" = "${foreground}";
      "dwm.fgdark" = "${black}";
      "dwm.accent" = "${magenta}";

      "dwm.font" = "monospace:pixelsize=16:style=Bold:antialias=true:autohint=true";
      "dwm.nerdfont" = "Symbols Nerd Font Mono:pixelsize=13:style=Bold:antialias=true:autohint=true";
      "dwm.hebfont" = "Noto Sans Hebrew:pixelsize=16:style=Bold:antialias=true:autohint=true";
      "dwm.emojifont" = "Noto Color Emoji:pixelsize=13:style=Bold:antialias=true:autohint=true";

      "dwm.borderpx" = 1;
      "dwm.gappx" = 9;
      "dwm.barheight" = 10;
      "dwm.baralpha" = 255;
    };
  };
}
