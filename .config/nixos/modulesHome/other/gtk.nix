{ config, lib, pkgs, ... }:

let
  cfg = config.custom.other.gtk;
in
{
  options.custom.other.gtk.enable = lib.mkEnableOption "enable gtk";

  config = lib.mkIf cfg.enable {
    gtk = {
      enable = true;

      theme.package = pkgs.gruvbox-gtk-theme;
      theme.name = "Gruvbox-Dark-BL";

      iconTheme.package = pkgs.whitesur-icon-theme;
      iconTheme.name = "WhiteSur-dark";

      gtk2.configLocation = "${config.xdg.configHome}/gtk-2.0/gtkrc";

      font.name = "sans-serif";
      font.size = 12;
    };
  };
}
