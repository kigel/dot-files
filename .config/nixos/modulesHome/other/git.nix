{ config, lib, pkgs, ... }:

let
  cfg = config.custom.other.git;
in
{
  options.custom.other.git.enable = lib.mkEnableOption "enable git";

  config = lib.mkIf cfg.enable {
    programs.git = {
      enable = true;
      userName = "kigel";
      userEmail = "itaykigelmain@gmail.com";
      extraConfig = {
        credential = { helper = "store"; };
      };
    };
  };
}
