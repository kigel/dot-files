{ lib, config, pkgs, inputs, ... }:

let
  cfg = config.custom.programs.sxhkd;
in
{
  options.custom.programs.sxhkd.enable = lib.mkEnableOption "enable sxhkd";

  config = lib.mkIf cfg.enable {
    services.sxhkd = {
      enable = true;
      extraConfig = '' ${builtins.readFile ./sxhkdrc} '';
    };
  };
}
