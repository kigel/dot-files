{ lib, config, pkgs, inputs, ... }:

let
  cfg = config.custom.programs.tmux;
in
{
  options.custom.programs.tmux.enable = lib.mkEnableOption "enable tmux";

  config = lib.mkIf cfg.enable {
    programs.tmux = {
      enable = true;
      baseIndex = 1;
      clock24 = true;
      disableConfirmationPrompt = true;
      historyLimit = 200000;
      keyMode = "vi";
      mouse = true;
      newSession = true;
      prefix = "C-b";
      plugins = with pkgs.tmuxPlugins; [
        sensible
        resurrect
        continuum
        {
          plugin = yank;
          extraConfig = '' 
            bind-key -T copy-mode-vi v send-keys -X begin-selection
            bind-key -T copy-mode-vi C-v send-keys -X rectangle-toggle
            bind-key -T copy-mode-vi y send-keys -X copy-selection-and-cancel
          '';
        }
      ];
      extraConfig = '' ${builtins.readFile ./tmux-extra.conf} '';
    };
  };
}

