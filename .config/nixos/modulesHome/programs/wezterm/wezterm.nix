{ lib, config, pkgs, inputs, ... }:

let
  cfg = config.custom.programs.wezterm;
in
{
  options.custom.programs.wezterm.enable = lib.mkEnableOption "enable wezterm";

  config = lib.mkIf cfg.enable {
    programs.wezterm = {
      enable = true;
      extraConfig = '' ${builtins.readFile ./wezterm.lua} '';
    };
  };
}
