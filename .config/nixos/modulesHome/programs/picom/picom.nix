{ lib, config, pkgs, inputs, ... }:

let
  cfg = config.custom.programs.picom;
in
{
  options.custom.programs.picom.enable = lib.mkEnableOption "enable picom";

  config = lib.mkIf cfg.enable {
    services.picom = {
      enable = true;
      activeOpacity = 1.0;
      inactiveOpacity = 1.0;
      menuOpacity = 1.0;
      backend = "glx";
      fade = true;
      fadeDelta = 5;
      fadeSteps = [ 0.03 0.03 ];
      vSync = true;
      shadow = false;
      settings = {
        blur = {
          method = "dual_kawase";
          strength = 7;
        };
      };
    };
  };
}
