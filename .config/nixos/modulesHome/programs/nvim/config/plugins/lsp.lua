local lspconfig = require("lspconfig")

local on_attach = function(_, bufnr)
	local nmap = function(keys, func, desc)
		if desc then
			desc = "LSP: " .. desc
		end

		vim.keymap.set("n", keys, func, { buffer = bufnr, desc = desc })
	end

	nmap("<leader>rn", vim.lsp.buf.rename, "[R]e[n]ame")
	nmap("<leader>gd", vim.lsp.buf.definition, "[G]oto [D]efinition")
	nmap("<leader>gr", require("telescope.builtin").lsp_references, "[G]oto [R]eferences")
	nmap("<leader>gi", vim.lsp.buf.implementation, "[G]oto [I]mplementation")
	nmap("<leader>dc", vim.lsp.buf.hover, "Hover [D]o[c]umentation")
end

-- Setup neovim lua configuration
require("neodev").setup({
	override = function(root_dir, library)
		if root_dir:find("/home/kigel/.config/nixos", 1, true) == 1 then
			library.enabled = true
			library.plugins = true
		end
	end,
})

require("fidget").setup()

-- nvim-cmp supports additional completion capabilities, so broadcast that to servers
local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities = require("cmp_nvim_lsp").default_capabilities(capabilities)

local language_servers = {
	gopls = {},
	lua_ls = {},
	rnix = {},
}

-- Initialize servers
for server, server_config in pairs(language_servers) do
	local config = { on_attach = on_attach }

	if server_config then
		for k, v in pairs(server_config) do
			config[k] = v
		end
	end

	lspconfig[server].setup(config)
end
