local custom_theme = require'lualine.themes.gruvbox-material'

-- Change the background of lualine_c section for normal mode
custom_theme.normal.c.bg = 'None'
custom_theme.insert.c.bg = 'None'
custom_theme.visual.c.bg = 'None'
custom_theme.replace.c.bg = 'None'
custom_theme.command.c.bg = 'None'
custom_theme.inactive.c.bg = 'None'

require("lualine").setup {
    options = {
        icons_enabled = true,
        theme = custom_theme,
        component_separators = "",
        section_separators = "",
        globalstatus = false,
    },
    sections = {
        lualine_a = {'mode'},
        lualine_b = {'branch', 'diff', 'diagnostics'},
        lualine_c = {'filename'},
        lualine_x = {'searchcount'},
        lualine_y = {'filetype'},
        lualine_z = {'location'}
    },
}
