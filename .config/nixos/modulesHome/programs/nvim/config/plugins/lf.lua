vim.g.lf_replace_netrw = 1
vim.g.lf_height = 35
vim.g.lf_width = 130

vim.keymap.set("n", "<C-o>", "<Cmd>Lf<CR>")

-- Open lf if nvim opens in a dir
if vim.fn.argc() == 0 then
	vim.defer_fn(function()
		vim.cmd("Lf")
	end, 0)
end
