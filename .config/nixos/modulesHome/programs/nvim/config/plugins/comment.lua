require('Comment').setup({
    -- -LHS of toggle mappings in NORMAL mode
    toggler = {
        ---Line-comment toggle keymap
        line = "cmm",
        ---Block-comment toggle keymap
        block = "cmb",
    },
    ---LHS of operator-pending mappings in NORMAL and VISUAL mode
    opleader = {
        ---Line-comment keymap
        line = "cm",
        ---Block-comment keymap
        block = "cb",
    },
    mappings = {
        extra = false,
    },
})
