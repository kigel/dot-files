local opts = { noremap = true, silent = true }

-- Shorten function name
local keymap = vim.keymap.set

vim.keymap.set("", "<Space>", "<Nop>", opts)
vim.g.mapleader = " "
vim.g.maplocalleader = " "

-- Modes
--   normal_mode = "n",
--   insert_mode = "i",
--   visual_mode = "v",
--   visual_block_mode = "x",
--   term_mode = "t",
--   command_mode = "c",

-- Global yanking
keymap("", "<leader>y", '"+y', opts)
keymap("", "<leader>Y", '"+Y', opts)
keymap("", "<leader>x", '"+x', opts)
keymap("", "<leader>X", '"+X', opts)

-- Stay in indent mode
keymap("v", "<", "<gv", opts)
keymap("v", ">", ">gv", opts)

-- Move text up and down
keymap("v", "<A-j>", ":m '>+1<CR>gv=gv", opts)
keymap("v", "<A-k>", ":m '>-2<CR>gv=gv", opts)

-- Save copy register when opts over text
keymap("v", "<leader>p", '"_dP', opts)

-- Maps for the cursor to stay in the middle
keymap("n", "<C-u>", "<C-u>zz", opts)
keymap("n", "<C-d>", "<C-d>zz", opts)
keymap("n", "n", "nzzzv", opts)
keymap("n", "N", "Nzzzv", opts)

-- Window managing
keymap("n", "<C-j>", "<C-w>j", opts)
keymap("n", "<C-k>", "<C-w>k", opts)
keymap("n", "<C-h>", "<C-w>h", opts)
keymap("n", "<C-l>", "<C-w>l", opts)

keymap("n", "<C-S-j>", "<C-w>j", opts)
keymap("n", "<C-S-k>", "<C-w>k", opts)
keymap("n", "<C-S-h>", "<C-w>h", opts)
keymap("n", "<C-S-l>", "<C-w>l", opts)

-- VSCode Maps
if vim.g.vscode then
	local vscode = require("vscode-neovim")

	-- Documentation
	vim.keymap.set("n", "<leader>dc", function()
		vscode.call("editor.action.showHover")
	end)

	-- Search Files
	vim.keymap.set("n", "<leader>sf", function()
		vscode.call("workbench.action.quickOpen")
	end)
	vim.keymap.set("n", "<leader>gf", function()
		vscode.call("workbench.action.quickOpen")
	end)

	-- VSCode Windows
	vim.keymap.set("n", "<leader>fi", function()
		vscode.call("workbench.view.explorer")
	end)
	vim.keymap.set("n", "<leader>git", function()
		vscode.call("workbench.view.scm")
	end)
	vim.keymap.set("n", "<leader>ex", function()
		vscode.call("workbench.view.extensions")
	end)
	vim.keymap.set("n", "<leader>j", function()
		vscode.call("workbench.action.togglePanel")
	end)
	vim.keymap.set("n", "<leader>b", function()
		vscode.call("workbench.action.toggleSidebarVisibility")
	end)
	vim.keymap.set("n", "<leader>s", function()
		vscode.call("workbench.action.openSettingsJson")
	end)
	vim.keymap.set("n", "<leader>z", function()
		vscode.call("workbench.action.toggleZenMode")
	end)

	vim.api.nvim_create_user_command("Format", function()
		vscode.call("editor.action.formatDocument")
	end, {})
end
