{ lib, config, pkgs, inputs, ... }:

let
  cfg = config.custom.programs.nvim;
in
{
  options.custom.programs.nvim.enable = lib.mkEnableOption "enable neovim";

  config = lib.mkIf cfg.enable {
    programs.neovim = {
      enable = true;

      viAlias = true;
      vimAlias = true;
      vimdiffAlias = true;

      extraPackages = with pkgs; [
        # LSP(s)
        lua-language-server
        rnix-lsp
        gopls

        # Formatters
        shfmt
        prettierd
        goimports-reviser
        nixfmt
        black
        stylua

        # DAP(s)
        delve
        python311Packages.debugpy

        xclip
        ripgrep
      ];

      plugins = with pkgs.vimPlugins; [
        # Telescope And File Managers
        telescope-nvim
        telescope-fzf-native-nvim
        nvim-web-devicons
        lf-vim

        # CMP
        nvim-cmp
        luasnip
        cmp_luasnip
        cmp-nvim-lsp
        friendly-snippets

        # Lualine
        lualine-nvim

        # Comment
        comment-nvim

        # LSP
        nvim-lspconfig
        fidget-nvim
        neodev-nvim

        # DAP
        nvim-dap
        nvim-dap-ui
        nvim-dap-virtual-text
        nvim-dap-go
        nvim-dap-python

        # Git
        vim-fugitive
        gitsigns-nvim

        # Format
        conform-nvim

        # Theme
        gruvbox-material

        # Colors
        nvim-colorizer-lua

        # Line on indents
        indent-blankline-nvim

        # Sudo Support
        suda-vim

        # Treesitter
        (nvim-treesitter.withPlugins (p: [
          p.tree-sitter-nix
          p.tree-sitter-vim
          p.tree-sitter-lua
          p.tree-sitter-bash
          p.tree-sitter-python
          p.tree-sitter-go
          p.tree-sitter-json
          p.tree-sitter-c
          p.tree-sitter-cpp
          p.tree-sitter-typescript
          p.tree-sitter-javascript
          p.tree-sitter-html
          p.tree-sitter-css
          p.tree-sitter-toml
          p.tree-sitter-yaml
          p.tree-sitter-git_config
          p.tree-sitter-rust
        ]))
        nvim-treesitter-textobjects
      ];

      extraLuaConfig = ''
        -- Base Settings
        ${builtins.readFile ./config/core/options.lua}
        ${builtins.readFile ./config/core/keymaps.lua}

        -- Plugins
        ${builtins.readFile ./config/plugins/treesitter.lua}
        ${builtins.readFile ./config/plugins/cmp.lua}
        ${builtins.readFile ./config/plugins/comment.lua}
        ${builtins.readFile ./config/plugins/lualine.lua}
        ${builtins.readFile ./config/plugins/lsp.lua}
        ${builtins.readFile ./config/plugins/format.lua}
        ${builtins.readFile ./config/plugins/git.lua}
        ${builtins.readFile ./config/plugins/indent_lines.lua}
        ${builtins.readFile ./config/plugins/telescope.lua}
        ${builtins.readFile ./config/plugins/lf.lua}
        ${builtins.readFile ./config/plugins/gruvbox-material.lua}
        ${builtins.readFile ./config/plugins/colorizer.lua}
        ${builtins.readFile ./config/plugins/dap.lua}
      '';
    };
  };
}

