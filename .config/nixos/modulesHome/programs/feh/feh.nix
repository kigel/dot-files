{ lib, config, pkgs, inputs, ... }:

let
  cfg = config.custom.programs.feh;
in
{
  options.custom.programs.feh.enable = lib.mkEnableOption "enable feh";

  config = lib.mkIf cfg.enable {
    programs.feh = {
      enable = true;
      buttons = {
        prev_img = "C-4";
        next_img = "C-5";
        zoom_in = 4;
        zoom_out = 5;
      };
    };
  };
}
