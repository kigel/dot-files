{ lib, config, pkgs, inputs, ... }:

let
  cfg = config.custom.programs.zsh;
in
{
  options.custom.programs.zsh.enable = lib.mkEnableOption "enable zsh";

  config = lib.mkIf cfg.enable {
    programs.zsh = {
      enable = true;
      dotDir = ".config/zsh";
      completionInit = " ";
      initExtraFirst = '' ${builtins.readFile ./zshrc} '';
      # envExtra = '' ${builtins.readFile ./zshenv} '';
    };

    home.file = {
      ".config/zsh/plugins/zsh-autosuggestions".source = "${pkgs.zsh-autosuggestions}/share/zsh-autosuggestions";
      ".config/zsh/plugins/zsh-fast-syntax-highlighting".source = "${pkgs.zsh-fast-syntax-highlighting}/share/zsh/site-functions";
      ".config/zsh/plugins/zsh-vi-mode".source = "${pkgs.zsh-vi-mode}/share/zsh-vi-mode";
    };
  };
}

