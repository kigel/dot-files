#  ____  __  __  __        __                                   
# |  _ \|  \/  | \ \      / / __ __ _ _ __  _ __   ___ _ __ ___ 
# | |_) | |\/| |  \ \ /\ / / '__/ _` | '_ \| '_ \ / _ \ '__/ __|
# |  __/| |  | |   \ V  V /| | | (_| | |_) | |_) |  __/ |  \__ \
# |_|   |_|  |_|    \_/\_/ |_|  \__,_| .__/| .__/ \___|_|  |___/
#                                    |_|   |_|                  

alias fzf_copy="fzf --bind \"enter:execute(echo {}; | xclip -selection clipboard)+abort\""

# Get the host distro (arch, debian, ubuntu, mint...)
host_distro="$(get-host-info distro)"
# Get the host type (pc or laptop)
host_type="$(get-host-info type)"

# pacman
if [ "$host_distro" = "arch" ]; then
    alias p="yay -S"
    alias psyu="yay -Syu"
    alias yeet="yay -Rss"
    # remote
    pss () { yay -Ss "$1" | awk '/^extra/ || /^core/ || /^community/ {sub(/.*\//, ""); sub(/ .*/, ""); print}' | fzf --preview 'yay -Si {}' | xargs -r yay -S --noconfirm }
    pssa () { yay -Ss "$1" | awk '/^aur/ {sub(/.*\//, ""); sub(/ .*/, ""); print}' | fzf --preview 'yay -Si {}' | xargs -r yay -S --noconfirm }
    # local
    pqq () { yay -Qq | fzf_copy --preview 'yay -Qil {}' }
    pqqa () { yay -Qqm | fzf_copy --preview 'yay -Qil {}' }

# nix <3
elif [ "$host_distro" = "nix" ]; then
    homeb () {
        home-manager switch --flake $XDG_CONFIG_HOME/nixos
    }
    nixb ()
    {
        sudo nixos-rebuild switch --flake $XDG_CONFIG_HOME/nixos
    }

# apt
elif [ "$host_distro" = "mint" ]; then
    alias p="sudo apt install --no-install-recommends"
    alias psyu="sudo apt update && sudo apt upgrade"
    alias yeet="sudo apt remove --auto-remove"
    # remote
    pss () {apt-cache search "$1" | awk '{print $1}' | fzf --preview 'apt show {}' | xargs -r sudo apt -y install --no-install-recommends}
    # local
    pqq () {apt list --installed | awk -F/ '{print $1}' | fzf_copy --preview 'apt show {}'}
fi

alias bins="cd $HOME/.local/bin"

alias byebye="shutdown -h now"

alias dogit='git --git-dir=$HOME/.dogit/ --work-tree=$HOME'
alias dogits='git --git-dir=$HOME/.dogit/ --work-tree=$HOME status'
alias dogita='git --git-dir=$HOME/.dogit/ --work-tree=$HOME add -u'
alias dogitc='git --git-dir=$HOME/.dogit/ --work-tree=$HOME commit -m'
alias dogitac='git --git-dir=$HOME/.dogit/ --work-tree=$HOME add -u && git --git-dir=$HOME/.dogit/ --work-tree=$HOME commit -m'
alias dogitu='git --git-dir=$HOME/.dogit/ --work-tree=$HOME fetch && git --git-dir=$HOME/.dogit/ --work-tree=$HOME pull'
alias dogitp='git --git-dir=$HOME/.dogit/ --work-tree=$HOME push'
alias dogitlazy='git --git-dir=$HOME/.dogit/ --work-tree=$HOME add -u && git --git-dir=$HOME/.dogit/ --work-tree=$HOME commit -m . && git --git-dir=$HOME/.dogit/ --work-tree=$HOME push'

alias gits='git status'
alias gita='git add .'
alias gitc='git commit -m'
alias gitac='git add . && git commit -m'
alias gitu='git fetch && git pull'
alias gitp='git push'
alias gitlazy='git add . && git commit -m . && git push'

alias cat="bat"

alias l="exa"
alias la="exa -A"
alias ll="exa -lh"
alias lla="exa -Alh"

alias img="feh -N --keep-zoom-vp -B #282828"

alias waf="cd $HOME/Repos/WAF/Waffle-Project"

#  _____ _   _ _   _  ____ _____ ___ ___  _   _ ____  
# |  ___| | | | \ | |/ ___|_   _|_ _/ _ \| \ | / ___| 
# | |_  | | | |  \| | |     | |  | | | | |  \| \___ \ 
# |  _| | |_| | |\  | |___  | |  | | |_| | |\  |___) |
# |_|    \___/|_| \_|\____| |_| |___\___/|_| \_|____/ 

config-fzf () {
    file=$(find $NIXOS_CONFIG -type f | fzf)
    if [ -n "$file" ]; then
        nvim -c "cd ~/.config/nixos" "${file}"
    fi
}
alias c="config-fzf"

projects-fzf () {
    if [ -n "$1" ]; then
        cd "$HOME/Repos/personal/$1"
        return 0
    fi

    selected_directory=$(find ~/Repos/personal -maxdepth 2 -type d | fzf)
    if [ -n "$selected_directory" ]; then
        cd $selected_directory
    fi
}

alias p="projects-fzf"

findfont () {
    fc-list | grep "$1" | awk -F': ' '{print $NF}' | sort
}

#  _____ __  __ _   ___  __
# |_   _|  \/  | | | \ \/ /
#   | | | |\/| | | | |\  / 
#   | | | |  | | |_| |/  \ 
#   |_| |_|  |_|\___//_/\_\

alias t="tmux-session-creator"

alias ts="tmux-switcher"

alias tp="tmux-sessionizer"

#  __  __ ___ ____   ____    ____ ___  _   _ _____ ___ ____
# |  \/  |_ _/ ___| / ___|  / ___/ _ \| \ | |  ___|_ _/ ___|
# | |\/| || |\___ \| |     | |  | | | |  \| | |_   | | |  _
# | |  | || | ___) | |___  | |__| |_| | |\  |  _|  | | |_| |
# |_|  |_|___|____/ \____|  \____\___/|_| \_|_|   |___\____|

setopt autocd nomatch notify
unsetopt beep extendedglob	# Disables beep sound
setopt autocd			# Automatically cd into typed directory.
stty stop undef			# Disable ctrl-s to freeze terminal.

#  ____  ____  _
# |  _ \/ ___|/ |
# | |_) \___ \| |
# |  __/ ___) | |
# |_|   |____/|_|

autoload -U colors && colors
autoload -Uz vcs_info
precmd_vcs_info() { vcs_info }
precmd_functions+=( precmd_vcs_info )
setopt prompt_subst
zstyle ':vcs_info:git:*' formats '  %b'

PS1='%B%{$fg[yellow]%}[%{$reset_color%}%B%{$fg[default]%}%n%{$reset_color%} %B%{$fg[cyan]%}%1~%{$reset_color%}%B%{$fg[yellow]%}]%{$reset_color%}%{$fg[magenta]%}${vcs_info_msg_0_}%{$reset_color%}%B > '

#  _   _ ___ ____ _____ ___  ______   __
# | | | |_ _/ ___|_   _/ _ \|  _ \ \ / /
# | |_| || |\___ \ | || | | | |_) \ V /
# |  _  || | ___) || || |_| |  _ < | |
# |_| |_|___|____/ |_| \___/|_| \_\|_|

if [ ! -d "$XDG_CACHE_HOME/zsh" ]; then
	mkdir "$XDG_CACHE_HOME/zsh"
fi

HISTFILE=$XDG_CACHE_HOME/zsh/histfile
HISTSIZE=500000
SAVEHIST=500000
setopt share_history

#     _   _   _ _____ ___     ____ ___  __  __ ____  _     _____ _____ _____
#    / \ | | | |_   _/ _ \   / ___/ _ \|  \/  |  _ \| |   | ____|_   _| ____|
#   / _ \| | | | | || | | | | |  | | | | |\/| | |_) | |   |  _|   | | |  _|
#  / ___ \ |_| | | || |_| | | |__| |_| | |  | |  __/| |___| |___  | | | |___
# /_/   \_\___/  |_| \___/   \____\___/|_|  |_|_|   |_____|_____| |_| |_____|

autoload -Uz compinit && compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots)		# Include hidden files.

ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="fg=245"

source $XDG_CONFIG_HOME/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
bindkey '^ ' autosuggest-accept

#  _     _____ ____ ____
# | |   |  ___/ ___|  _ \
# | |   | |_ | |   | | | |
# | |___|  _|| |___| |_| |
# |_____|_|   \____|____/

function lfcd ()
{
    tmp="$(mktemp -uq)"
    trap 'rm -f $tmp >/dev/null 2>&1 && trap - HUP INT QUIT TERM PWR EXIT' HUP INT QUIT TERM PWR EXIT
    lf -last-dir-path="$tmp" "$@"
    if [ -f "$tmp" ]; then
        dir="$(cat "$tmp")"
        [ -d "$dir" ] && [ "$dir" != "$(pwd)" ] && cd "$dir"
    fi
}

bindkey -s '^o' '^ulfcd\n'

source $XDG_CONFIG_HOME/zsh/plugins/zsh-fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh
source $XDG_CONFIG_HOME/zsh/plugins/zsh-vi-mode/zsh-vi-mode.plugin.zsh
