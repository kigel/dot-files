{
  imports = [
    ./programs/wezterm/wezterm.nix
    ./programs/lf/lf.nix
    ./programs/feh/feh.nix
    ./programs/picom/picom.nix
    ./programs/sxhkd/sxhkd.nix
    ./programs/nvim/nvim.nix
    ./programs/zsh/zsh.nix
    ./programs/tmux/tmux.nix
    ./other/xrdb.nix
    ./other/git.nix
    ./other/gtk.nix
  ];
}
