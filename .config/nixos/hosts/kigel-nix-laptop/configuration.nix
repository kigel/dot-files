{ config, pkgs, ... }:

{
  imports = [
    ./hardware-configuration.nix
    ./../../nixos.nix
  ];

  boot.kernelParams = [ "intel_idle.max_cstate=1" ];

  networking.hostName = "kigel-nix-laptop";

  # networking.wireless.enable = true;
  # networking.wireless.userControlled.enable = true;
  # networking.wireless.networks."Kigel 1" = {
  #     psk = "0507928177";
  # };
  networking.networkmanager.enable = true;

  services.logind.lidSwitch = "ignore";
  services.logind.lidSwitchDocked = "ignore";
  services.logind.lidSwitchExternalPower = "ignore";

  # services.xserver.desktopManager.cinnamon.enable = true;

  environment.systemPackages = with pkgs; [
    networkmanager_dmenu
  ];

  # DON'T CHANGE
  system.stateVersion = "23.11";
}
