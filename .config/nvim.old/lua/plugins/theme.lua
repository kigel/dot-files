return {
	"sainnhe/gruvbox-material",
	lazy = false,
	priority = 1000,
	config = function()
		vim.g.gruvbox_material_foreground = "material"
		vim.g.gruvbox_material_transparent_background = 1
		vim.g.gruvbox_material_float_style = "bright"
		vim.o.background = "dark"
		vim.cmd.colorscheme("gruvbox-material")
	end,
}

-- return {
--     "catppuccin/nvim",
--     name = "catppuccin",
--     priority = 1000,
-- 	config = function()
--         require("catppuccin").setup({
--             flavour = "mocha",
--             transparent_background = true,
--         })
--
-- 		vim.cmd.colorscheme("catppuccin")
-- 	end,
-- }

-- return {
--   "neanias/everforest-nvim",
--   version = false,
--   lazy = false,
--   priority = 1000,
--   config = function()
--     require("everforest").setup({
--         background = "hard",
--     })
--
--     vim.cmd.colorscheme("everforest")
--   end,
-- }
