return {
	"neovim/nvim-lspconfig",
	dependencies = {
		{
			"williamboman/mason.nvim",
			config = true,
		},

		"WhoIsSethDaniel/mason-tool-installer.nvim",

		{
			"williamboman/mason-lspconfig.nvim",
		},

		{
			"j-hui/fidget.nvim",
			tag = "legacy",
			opts = {},
		},

		{
			"folke/neodev.nvim",
		},
	},
	config = function()
		local on_attach = function(_, bufnr)
			-- In this case, we create a function that lets us more easily define mappings specific
			-- for LSP related items. It sets the mode, buffer and description for us each time.
			local nmap = function(keys, func, desc)
				if desc then
					desc = "LSP: " .. desc
				end

				vim.keymap.set("n", keys, func, { buffer = bufnr, desc = desc })
			end

			nmap("<leader>rn", vim.lsp.buf.rename, "[R]e[n]ame")
			nmap("<leader>gd", vim.lsp.buf.definition, "[G]oto [D]efinition")
			nmap("<leader>gr", require("telescope.builtin").lsp_references, "[G]oto [R]eferences")
			nmap("<leader>gi", vim.lsp.buf.implementation, "[G]oto [I]mplementation")
			nmap("<leader>dc", vim.lsp.buf.hover, "Hover [D]o[c]umentation")
		end

		-- Setup neovim lua configuration
		require("neodev").setup()

		-- nvim-cmp supports additional completion capabilities, so broadcast that to servers
		local capabilities = vim.lsp.protocol.make_client_capabilities()
		capabilities = require("cmp_nvim_lsp").default_capabilities(capabilities)

		-- Ensure the servers above are installed
		local mason_lspconfig = require("mason-lspconfig")

		mason_lspconfig.setup({
			ensure_installed = {},
		})

		mason_lspconfig.setup_handlers({
			function(server_name)
				require("lspconfig")[server_name].setup({
					capabilities = capabilities,
					on_attach = on_attach,
				})
			end,
		})
	end,
}
