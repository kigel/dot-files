return {
	"lmburns/lf.nvim",
	commit = "383429497292dd8a84271e74a81c6db6993ca7ab",
	lazy = false,
	dependencies = {
		"akinsho/toggleterm.nvim",
		version = "*",
		config = true,
	},
	config = function()
		-- This feature will not work if the plugin is lazy-loaded
		vim.g.lf_netrw = 1

		require("lf").setup({
			default_file_manager = false, -- make lf default file manager
			disable_netrw_warning = true, -- don't display a message when opening a directory with `default_file_manager` as true
			escape_quit = false,
			border = "rounded",
			winblend = 0,
		})

		vim.keymap.set("n", "<C-o>", "<Cmd>Lf<CR>")

		-- Open lf if nvim opens in a dir
		if vim.fn.argc() == 0 then
			vim.defer_fn(function()
				vim.cmd("Lf")
			end, 0)
		end
	end,
}
