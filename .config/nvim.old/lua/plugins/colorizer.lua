return {
	"norcalli/nvim-colorizer.lua",
	lazy = false,
	opts = {
		"*",
		cpp = {
			names = false,
		},
	},
}
