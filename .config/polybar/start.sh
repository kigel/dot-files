#!/bin/sh

# If all your bars have ipc enabled, you can also use
polybar-msg cmd quit

if [ ! -d "$XDG_CACHE_HOME/polybar" ]; then
	mkdir "$XDG_CACHE_HOME/polybar"
fi

polybar mainbar 2>"$XDG_CACHE_HOME"/polybar/polybar.log &
polybar secondbar 2>"$XDG_CACHE_HOME"/polybar/polybar.log &
disown
